﻿using System;
using System.Collections.Generic;

namespace QuizBot.Model
{
    [Serializable]
    public class Question
    {
        public int Index { get; set; }
        public string Text { get; set; }

        public List<string> Answers { get; set; }

        public string CorrectAnswer { get; set; }
    }
}