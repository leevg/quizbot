﻿using System.Collections.Generic;

namespace QuizBot.Model
{
    public class QuestionRoot
    {
        public List<Question> Questions { get; set; }
    }
}