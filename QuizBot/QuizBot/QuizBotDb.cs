﻿using QuizBot.Model;
using System.Data.Entity;

namespace QuizBot
{
    public partial class QuizBotDb : DbContext
    {

        public QuizBotDb()
            : base("name=QuizBotDb")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<PersonalData> PersonalDatas { get; set; }
        public DbSet<AnswerStatistic> AnswerStatistics { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}