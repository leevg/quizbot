﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using QuizBot.Services;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace QuizBot.Dialogs
{
    [Serializable]
    public class MainDialog : IDialog<string>
    {
        private bool _onExit;
        public MainDialog(bool onExit = false)
        {
            _onExit = onExit;
        }
        async Task IDialog<string>.StartAsync(IDialogContext context)
        {
            context.Wait<string>(MessageReceived);
        }

        private async Task MessageReceived(IDialogContext context, IAwaitable<string> message)
        {
            var user = DataProviderService.Instance.GetUser(context.Activity.From.Id);
            if (user == null)
            {
                await context.PostAsync("Hello! My name is QuizBot and what's your name?");
                context.Wait(Resume);
            }
            else
            {
                if (_onExit)
                {
                    _onExit = false;
                    await OnExit(context);
                }
                else
                {
                    await context.PostAsync($"Hi, {user.Name}! Let's start!");
                    await StartQuiz(context);
                }
            }
        }

        private async Task Resume(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var activity = await result;
            if (!string.IsNullOrWhiteSpace(activity.Text))
            {
                var text = new StringBuilder();
                text.AppendLine($"Nice to meet you, {activity.Text}, let's test your HTML skills");                
                await context.PostAsync(text.ToString());
                DataProviderService.Instance.AddUser(activity.From.Id, activity.Text);
                await StartQuiz(context);
            }
            else
            {
                await context.PostAsync("Your name is uncommon,  I can't spell it.");
            }
        }

        private async Task StartQuiz(IDialogContext context)
        {
            await context.Forward(new QuizDialog(), QuizComplete, "", CancellationToken.None);
        }

        private async Task QuizComplete(IDialogContext context, IAwaitable<string> result)
        {
            await OnExit(context);
        }

        private async Task OnExit(IDialogContext context)
        {
            var user = DataProviderService.Instance.GetUser(context.Activity.From.Id);
           
            await context.PostAsync($"{user.Name}, it was fun, come again later!");
            DataProviderService.Instance.ClearAnswerStatistics(context.Activity.From.Id);
           
            context.Done("");
        }
    }
}