﻿using System.Collections.Generic;
using System.Web.Http;
using QuizBot.Model;
using QuizBot.Services;

namespace QuizBot.Controllers
{
   
    public class RegistrationController : ApiController
    {
        public List<PrinterUser> GetNewUsers() => DataProviderService.Instance.GetNewUsers();
    }
}
