﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizBot
{
    public class Consts
    {
        public static readonly string[] CorrectAnswers = { "Correct!", "Yes!", "Sure!", "You are right!" };
        public static readonly string[] BadAnswers = { "Wrong", "No", "Unfortunately you are wrong" };
        public static readonly string[] ExitCommands = { "/exit", "bored", "stop it", "finish", "end", "done", "stop", "exit" };

        public static readonly string[] GoodStickers = { "CAADAgADNAADmtX9DYR2kzhi5vtCAg", "CAADAgADOQADmtX9DcUaRCUMG8FyAg", "CAADAgADOgADmtX9DSsFSsoo5OXHAg" };
        public static readonly string[] BadStickers = { "CAADAgADNQADmtX9DTsQHyC9ciI0Ag", "CAADAgADNgADmtX9Dam0M3XDE7yUAg", "CAADAgADNwADmtX9DVA6KI51zmERAg", "CAADAgADOAADmtX9DcmamIZIYfS7Ag" };

    }
}